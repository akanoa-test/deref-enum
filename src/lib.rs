pub use deref_enum_macro::DerefEnum;
use std::any::Any;

pub trait DerefEnum
where
    Self: 'static,
{
    fn get_inner_ref<T: DerefEnum + 'static>(&self) -> Option<&T>
    where
        Self: Sized,
    {
        (self as &dyn Any).downcast_ref()
    }

    fn get_inner_mut<T: DerefEnum + 'static>(&mut self) -> Option<&mut T>
    where
        Self: Sized,
    {
        (self as &mut dyn Any).downcast_mut()
    }
}

fn playground() {
    #[derive(DerefEnum)]
    struct Network {
        qos: u8,
    }
    impl Network {
        fn set_qos(&mut self, qos: u8) {
            self.qos = qos
        }
    }

    #[derive(DerefEnum)]
    struct File;

    #[derive(DerefEnum)]
    enum WriteOn {
        Network(Network),
        File(File),
    }

    fn deref_mut_write_on(writter: &mut WriteOn) {
        let mut network_writter = writter.get_inner_mut::<Network>().expect("fdg");
    }

    let mut write_on = WriteOn::Network(Network { qos: 4 });

    deref_mut_write_on(&mut write_on)
}
