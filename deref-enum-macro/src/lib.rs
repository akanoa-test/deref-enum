use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{Data, DataEnum, DeriveInput, Variant};

#[proc_macro_derive(DerefEnum)]
pub fn deref_enum_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as DeriveInput);

    match &ast.data {
        Data::Struct(_) => impl_deref_enum_macro_derive_struct(&ast),
        Data::Enum(enum_data) => {
            let variants = parse_enum_data(enum_data);
            impl_deref_enum_macro_derive_enum(&ast.ident, variants)
        }
        Data::Union(_) => {
            unimplemented!("EnumDeref not implemented for Union")
        }
    }
}

fn impl_deref_enum_macro_derive_struct(ast: &DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl DerefEnum for #name {}
    };
    gen.into()
}

fn impl_deref_enum_macro_derive_enum(name: &syn::Ident, variants: Vec<syn::Ident>) -> TokenStream {
    let body_mut = impl_body_enum(name, &variants, format_ident!("get_inner_mut"));
    let body_ref = impl_body_enum(name, &variants, format_ident!("get_inner_ref"));
    let gen = quote! {

            impl DerefEnum for #name {

                fn get_inner_ref<T: DerefEnum + 'static>(&self) -> Option<&T>
                where Self: Sized,
            {
                match self {
                    #body_ref
                }
            }

                fn get_inner_mut<T: DerefEnum + 'static>(&mut self) -> Option<&mut T>
                where Self: Sized,
            {
                match self {
                    #body_mut
                }
            }
        }
    };
    gen.into()
}

fn impl_body_enum(
    name: &syn::Ident,
    variants: &Vec<syn::Ident>,
    method: syn::Ident,
) -> proc_macro2::TokenStream {
    let mut body = quote!();
    for variant in variants {
        body = quote! {
            #body
            #name::#variant(inner) => inner.#method(),
        }
    }
    body
}

fn parse_enum_data(data: &DataEnum) -> Vec<syn::Ident> {
    data.variants.iter().map(parse_enum_variant).collect()
}

fn parse_enum_variant(variant: &Variant) -> syn::Ident {
    let ident = variant.ident.clone();
    let mut fields = variant.fields.iter();
    let _field = fields
        .next()
        .expect("Named deref_enum variant must have at least one unnamed field");
    if fields.next().is_some() {
        panic!("Enum variants can only have one unnamed field");
    }
    ident
}
